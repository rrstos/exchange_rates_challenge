package com.br.exchangerate.modules

import com.br.exchangerate.qualifiers.ExchangeRates
import com.br.exchangerate.webservice.ExchangeRatesApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Robson on 2019-06-26.
 */
@Module
class ApisModule {

    @Provides
    @Singleton
    fun providesExchangesRateApi(@ExchangeRates retrofit: Retrofit): ExchangeRatesApi =
        retrofit.create(ExchangeRatesApi::class.java)
}