package com.br.exchangerate

import com.br.exchangerate.domain.ExchangeRatesEntity
import com.br.exchangerate.webservice.ExchangeRatesApi
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by Robson on 2019-06-26.
 */
class MainInteractor @Inject constructor() {

    @Inject
    lateinit var api: ExchangeRatesApi

    fun getList(): Single<ExchangeRatesEntity> = api.getList()
}