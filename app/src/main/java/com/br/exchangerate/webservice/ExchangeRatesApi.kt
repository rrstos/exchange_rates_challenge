package com.br.exchangerate.webservice

import com.br.exchangerate.domain.ExchangeRatesEntity
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by Robson on 2019-06-26.
 */
interface ExchangeRatesApi {

    @GET("latest")
    fun getList(): Single<ExchangeRatesEntity>

}