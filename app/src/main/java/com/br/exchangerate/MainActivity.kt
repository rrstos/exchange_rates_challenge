package com.br.exchangerate

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import butterknife.ButterKnife
import com.br.exchangerate.domain.RatesEntity
import com.br.exchangerate.extensions.error
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.loading.*
import java.math.BigDecimal
import javax.inject.Inject

/**
 * Created by Robson on 2019-06-26.
 */
class MainActivity : AppCompatActivity(), MainView {

    @Inject
    lateinit var presenter: MainPresenter

    private val appComponent: AppComponent
        get() {
            val application = application as ExchangeRatesApplication

            return application.appComponent ?: throw IllegalStateException()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        ButterKnife.bind(this)

        appComponent.inject(this)

        presenter.attachView(this)
    }

    override fun onDestroy() {
        super.onDestroy()

        presenter.detachView()
    }

    override fun getExchangeRatesList(rates: RatesEntity?) {

        text_usd_rate.text = getString(R.string.main_usd_rate, rates?.usd ?: BigDecimal.ZERO)

        text_pln_rate.text = getString(R.string.main_pln_rate, rates?.pln ?: BigDecimal.ZERO)
    }

    override fun showError(message: String?) {
        error(message)
        setTextsVisible(on = false)
    }

    override fun startLoading() {
        loading_frame.visibility = View.VISIBLE
        setTextsVisible(on = false)
    }

    override fun endLoading() {
        loading_frame.visibility = View.GONE
        setTextsVisible(on = true)
    }

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    private fun setTextsVisible(on: Boolean) {

        if (on) {
            text_usd_rate.visibility = View.VISIBLE
            text_pln_rate.visibility = View.VISIBLE
        } else {
            text_usd_rate.visibility = View.INVISIBLE
            text_pln_rate.visibility = View.INVISIBLE
        }
    }
}
