package com.br.exchangerate

import android.os.Handler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


/**
 * Created by Robson on 2019-06-26.
 */
class MainPresenter @Inject constructor() {

    private val timerInterval: Long = 60000//one minute

    private val timeHandler: Handler = Handler()

    @Inject
    internal lateinit var interactor: MainInteractor

    private var view: MainView? = null

    private var disposable: Disposable? = null

    private var runnableTimer: Runnable? = null

    fun attachView(view: MainView) {
        this.view = view

        createTimer()

        timeHandler.post(runnableTimer)
    }

    fun detachView() {
        this.view = null

        disposable?.dispose()

        disposable = null

        timeHandler.removeCallbacks(runnableTimer)
    }

    private fun getExchangeRatesList() {
        view?.startLoading()

        disposable = interactor.getList()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ list ->

                view?.getExchangeRatesList(list.rates)

                view?.endLoading()

            }) { throwable ->

                view?.endLoading()

                val message: String? = throwable.message

                view?.showError(message)
            }
    }

    private fun createTimer() {

        runnableTimer = object : Runnable {

            override fun run() {
                getExchangeRatesList()

                timeHandler.postDelayed(this, timerInterval)
            }
        }

    }

}