package com.br.exchangerate.qualifiers

import javax.inject.Qualifier

/**
 * Created by Robz on 25/03/2018.
 */
@Qualifier
@Target(
    AnnotationTarget.FUNCTION,
    AnnotationTarget.PROPERTY_GETTER,
    AnnotationTarget.PROPERTY_SETTER,
    AnnotationTarget.VALUE_PARAMETER
)
@Retention(AnnotationRetention.RUNTIME)
annotation class ExchangeRates