package com.br.exchangerate.domain

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

/**
 * Created by Robson on 2019-06-26.
 */
class RatesEntity {

    @SerializedName("USD")
    var usd: BigDecimal? = null

    @SerializedName("PLN")
    var pln: BigDecimal? = null

}