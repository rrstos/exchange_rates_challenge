package com.br.exchangerate

import android.app.Application
import com.br.exchangerate.modules.AndroidModule

/**
 * Created by Robson on 2019-06-26.
 */
class ExchangeRatesApplication : Application() {

    var appComponent: AppComponent? = null
        private set

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
            .androidModule(AndroidModule(this))
            .build()

    }
}