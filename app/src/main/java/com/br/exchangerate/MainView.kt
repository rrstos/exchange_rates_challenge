package com.br.exchangerate

import com.br.exchangerate.domain.RatesEntity

/**
 * Created by Robson on 2019-06-26.
 */
interface MainView {

    fun showError(message: String?)

    fun getExchangeRatesList(rates: RatesEntity?)

    fun startLoading()

    fun endLoading()

}