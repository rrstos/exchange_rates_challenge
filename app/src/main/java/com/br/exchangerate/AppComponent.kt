package com.br.exchangerate

import com.br.exchangerate.modules.*
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Robson on 2019-06-26.
 */
@Component(modules = [AndroidModule::class, RetrofitModule::class, ApisModule::class, OkHttpClientModule::class, GsonModule::class])
@Singleton
interface AppComponent {

    fun inject(mainActivity: MainActivity)

}